import React from "react";

export default ({ message }) =>
    <div className="error-content">
        <div>Oops!</div>
        <span>Something is wrong!</span>
        {message || "Some error happened"}
    </div>;
