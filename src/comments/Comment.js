import React from "react";
import PropTypes from "prop-types";

const Comment = ({ id, name, body, email }) => {
  return (
          <div className="comment">
              <h5 className="comment-name">{name}</h5>
              <div className="comment-body">{body}</div>
              <div className="comment-email">{email}</div>
          </div>
  );
};

Comment.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired
};

export default Comment;
