import React from "react";
import PropTypes from "prop-types";
import Comment from "./Comment";

const CommentsList = ({ comments }) => {
  return (
    <div>
      {comments.map(c => (
        <Comment key={c.id} {...c} />
      ))}
    </div>
  );
};

CommentsList.propTypes = {
  comments: PropTypes.array.isRequired
};

export default CommentsList;
