import React from "react";
import Loader from "../lib/Loader";
import CommentsList from "./CommentsList";
import Loading from "../components/Loading";
import Error from "../components/Error";
import PropTypes from "prop-types";
import "./comment.css";
import Constant from "../lib/Constant";

class CommentsContainer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      comments: [],
      loaded: false,
      error: null
    };
  }

  componentDidMount() {
    const { postId } = this.props;
    Loader.loadComments()
      .then(comments => {
        this.setState({
          comments: comments
            .filter(c => c.postId === postId)
            .splice(0, Constant.COMMENTS_LIMIT),
          loaded: true
        });
      })
      .catch(e => {
        this.setState({ loaded: false, error: "Some error while loading" });
      });
  }

  render() {
    const { comments, loaded, error } = this.state;

    if (error) {
      return <Error message={error} />;
    }

    if (loaded) {
      return (
        <div>
            <h5 className="comment-title">Comments</h5>
          <CommentsList comments={comments} />
        </div>
      );
    }

    return <Loading />;
  }
}

CommentsContainer.propTypes = {
  postId: PropTypes.number.isRequired
};

export default CommentsContainer;
