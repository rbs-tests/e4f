const Constant = {
  POSTS_URL:
    "https://gist.githubusercontent.com/stackdumper/7048df6cbf9cd5c09eb7e5bca4ee292e/raw/e423f18809c6cacf720bb8b9fe227e14f4453fbf/posts.json",
  COMMENTS_URL:
    "https://gist.githubusercontent.com/stackdumper/870afde831ca6a202796bf651e9e4696/raw/cba8ee3ff99922b54730cc19886a9a9f2a3c5fae/comments.json",
  LIMIT: 10,
  COMMENTS_LIMIT: 3
};
export default Object.assign({}, Constant);
