import Constant from "./Constant";
import axios from "axios";

export default class Loader {
  static loadPosts() {
    if (localStorage.getItem("posts")) {
      return Promise.resolve(JSON.parse(localStorage.getItem("posts")));
    }

    return (
      localStorage.getItem("posts") ||
      axios.get(Constant.POSTS_URL).then(({ data: posts }) => {
        localStorage.setItem("posts", JSON.stringify(posts));
        return posts;
      })
    );
  }

  static loadComments() {
    if (localStorage.getItem("comments")) {
      return Promise.resolve(JSON.parse(localStorage.getItem("comments")));
    }
    return axios.get(Constant.COMMENTS_URL).then(({ data: comments }) => {
      localStorage.setItem("comments", JSON.stringify(comments));
      return comments;
    });
  }
}
