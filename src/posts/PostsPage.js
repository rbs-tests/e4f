import React from "react";
import Loader from "../lib/Loader";
import Constant from "../lib/Constant";
import PostsList from "./PostsList";
import Loading from "../components/Loading";
import Error from "../components/Error";
import "./post.css";

export default class PostsPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      page: 0,
      posts: [],
      hasNext: false,
      hasPrev: false,
      loaded: false,
      error: null
    };

    this.getCurrentPosts = this.getCurrentPosts.bind(this);
    this.paginate = this.paginate.bind(this);
  }

  componentDidMount() {
    Loader.loadPosts()
      .then(posts => {
        this.setState({
          posts: posts,
          hasNext: posts.length > Constant.LIMIT,
          loaded: true
        });
      })
      .catch(e => {
        this.setState({ loaded: false, error: "Some error while loading" });
      });
  }

  paginate(pageStep) {
    let { page, posts } = this.state;
    let maxPage = Math.ceil(posts.length / Constant.LIMIT) - 1;
    let hasNext = false;
    let hasPrev = false;
    page += pageStep;

    if (page < 0) {
      page = 0;
    } else if (page > maxPage) {
      page = maxPage;
    }

    if (page > 0) {
      hasPrev = true;
    }

    if (page < maxPage) {
      hasNext = true;
    }

    this.setState({ page, hasNext, hasPrev });
  }

  getCurrentPosts() {
    const { page, posts } = this.state;
    let skip = page * Constant.LIMIT;
    let currentPosts = [];
    for (let i = skip; i < skip + Constant.LIMIT && i < posts.length; i++) {
      currentPosts.push(posts[i]);
    }
    return currentPosts;
  }

  render() {
    const { page, hasNext, hasPrev, loaded, error } = this.state;

    if (error) {
      return <Error message={error} />;
    }

    if (loaded) {
      return (
        <div>
          <PostsList posts={this.getCurrentPosts()} />

          <div className="pagination container flex ai-c">
            {hasPrev && (
              <div
                className="prev flex jc-c ai-c mr-20"
                onClick={() => this.paginate(-1)}
              >
                Previous
              </div>
            )}
            <div className="pagination-page-number flex jc-c ai-c mr-20">
              {page + 1}
            </div>
            {hasNext && (
              <div
                className="next flex jc-c ai-c"
                onClick={() => this.paginate(1)}
              >
                Next
              </div>
            )}
          </div>
        </div>
      );
    }

    return <Loading />;
  }
}
