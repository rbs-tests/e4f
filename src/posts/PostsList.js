import React from "react";
import PropTypes from "prop-types";
import Post from "./Post";

const PostsList = ({ posts }) => {
  return (
    <div  className="container pt-30">
      {posts.map(p => (
        <Post key={p.id} {...p} />
      ))}
    </div>
  );
};

PostsList.propTypes = {
  posts: PropTypes.array.isRequired
};

export default PostsList;
