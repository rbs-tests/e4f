import React, { useState } from "react";
import PropTypes from "prop-types";
import CommentsContainer from "../comments/CommentsContainer";

const Post = ({ id, title, body }) => {
  const [visibleComments, setVisibleComments] = useState(false);
  return (
    <div className="post">
      <h3 className="post-title" onClick={() => setVisibleComments(true)}>{title}</h3>
      <div className="post-body">{body}</div>
      {visibleComments && (
        <div>
          <CommentsContainer postId={id} />
          <div className="next flex jc-c ai-c" onClick={() => setVisibleComments(false)}>Close</div>
        </div>
      )}
    </div>
  );
};

Post.propTypes = {
  id: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired
};

export default Post;
