import React from "react";
import PostsPage from "./posts/PostsPage";

function App() {
  return (
    <div>
      <PostsPage />
    </div>
  );
}

export default App;
