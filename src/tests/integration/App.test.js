import React from "react";

import App from "../../App";
import PostsPage from "../../posts/PostsPage";
import Post from "../../posts/Post";
import PostsList from "../../posts/PostsList";
import CommentsList from "../../comments/CommentsList";
import CommentsContainer from "../../comments/CommentsContainer";
import Comment from "../../comments/Comment";
import posts from "./posts.json";
import comments from "./comments.json";
import { shallow } from "enzyme";

const mockLoadPosts = jest.fn();
mockLoadPosts.mockResolvedValue(posts);

const mockLoadComments = jest.fn();

mockLoadComments.mockResolvedValue(comments);

jest.mock(
  "../../lib/Loader",
  () =>
    class {
      static loadPosts() {
        return mockLoadPosts();
      }
      static loadComments() {
        return mockLoadComments();
      }
    }
);

describe("all tests", () => {
  beforeEach(() => {
    // Clear all instances and calls to constructor and all methods:
    mockLoadPosts.mockClear();
    mockLoadComments.mockClear();
  });

  it("renders without crashing", () => {
    shallow(<App />);
  });

  it("should contain posts page", async () => {
    let wrap = shallow(<App />);
    await delay(10);
    expect(wrap.contains(<PostsPage />)).toEqual(true);
  });

  it("should contain correct props and load posts", async () => {
    let wrap = shallow(<PostsPage />);
    await delay(10);
    expect(wrap.state("posts")).toHaveLength(100);
    expect(wrap.state("hasNext")).toEqual(true);
    expect(wrap.state("loaded")).toEqual(true);
    expect(mockLoadPosts.mock.calls).toHaveLength(1);
  });

  it("should contain posts list with start page", async () => {
    let wrap = shallow(<PostsPage />);
    await delay(100);
    expect(wrap.state("page")).toEqual(0);
    expect(
      wrap.contains(<PostsList posts={[...posts].splice(0, 10)} />)
    ).toEqual(true);
  });

  it("should paginate right", async () => {
    let wrap = shallow(<PostsPage />);
    await delay(100);

    wrap.find(".next").simulate("click");
    await delay(10);
    expect(wrap.state("page")).toEqual(1);
    expect(
      wrap.contains(<PostsList posts={[...posts].splice(10, 10)} />)
    ).toEqual(true);
    wrap.find(".prev").simulate("click");
    await delay(10);
    expect(wrap.state("page")).toEqual(0);
    expect(wrap.find(".prev")).toHaveLength(0);
    expect(wrap.find(".next")).toHaveLength(1);

    for (let i = 0; i < 9; i++) {
      wrap.find(".next").simulate("click");
      await delay(10);
    }

    expect(wrap.state("page")).toEqual(9);
    expect(wrap.find(".next")).toHaveLength(0);
  });

  it("should show comments correct and load them", async () => {
    let wrap = shallow(<PostsPage />);
    await delay(100);

    let postList = wrap.find(PostsList).shallow();

    let post = postList
      .find(Post)
      .first()
      .shallow();

    post.find(".post-title").simulate("click");

    let commentsContainer = post.find(CommentsContainer).shallow();
    await delay(100);
    let commentsList = commentsContainer.find(CommentsList).shallow();

    let comments = commentsList.find(Comment);
    expect(mockLoadComments.mock.calls).toHaveLength(1);
    expect(comments).toHaveLength(3);
  });
});

function delay(d) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve();
    }, d);
  });
}
